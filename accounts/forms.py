from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm, Form
from registration.forms import RegistrationForm
from registration.users import UsernameField

from accounts.models import Member


class RegisterForm(ModelForm):
    invitee_invitation_code = forms.CharField(max_length=5, label="کد معرف", required=False)

    class Meta:
        model = Member
        fields = (UsernameField(),)

    def clean_invitee_invitation_code(self):
        invitation_code = self.cleaned_data['invitee_invitation_code']
        if invitation_code != '':
            try:
                Member.objects.get(invitation_code=invitation_code)
            except Member.DoesNotExist:
                raise ValidationError("کد معرف واردشده نامعتبر است.")
        return invitation_code


class CodeForm(Form):
    mobile = forms.CharField(max_length=11, required=True)
