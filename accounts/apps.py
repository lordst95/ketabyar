from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'accounts'
    verbose_name = "My Application VerboseName Test"

    def ready(self):
        from accounts.models import Member
        try:
            Member.objects.get(username='09102030657')
        except Member.DoesNotExist:
            Member.objects.create_superuser(username='09102030657', password='sinasina20',
                                            email='sina.tamjidy@gmail.com')
            member = Member.objects.get(username='09102030657')
            member.set_password('sinasina20')
            member.save()
        except:
            pass

