from django.contrib import admin

# Register your models here.
from accounts.models import Member

admin.site.register(Member)
