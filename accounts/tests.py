from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.status import HTTP_200_OK

from FinancialManagement.models import CreditCard
from accounts.models import Member


class AccountsTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.username = '09102030657'
        cls.password = 'gates'
        cls.email = 'johnny@gmail.com'

    # def setUp(self):
    #     super().setUp()
    #     user = Member.objects.create_superuser(username=self.username, password=self.password,
    #                                            email=self.email)
    #     CreditCard.objects.get_or_create(assosiated_user=user)

    # TODO badan ie test vase signup ba invitation code bezan

    def test_signup(self):
        url = reverse('registration_register')
        data = {
            'username': self.username
        }
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Member.objects.get(username=data['username']).username, data['username'])

    def test_login_valid(self):
        url = reverse('registration_login')
        new_user = Member.objects.create(username=self.username)
        new_user.set_password(self.password)
        new_user.save()
        data = {
            'username': self.username,
            'password': self.password
        }
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertRedirects(response, reverse('book:book-list'))

    def test_login_invalid(self):
        url = reverse('registration_login')
        data = {
            'username': self.username,
            'password': self.password
        }
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.context['request'].path_info, reverse('registration_login'))

    def test_edit_profile_invitation_code(self):
        url = reverse('edit-profile')
        Member.objects.create_superuser(username=self.username, password=self.password,
                                        email=self.email)
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(url, follow=True)
        self.assertEqual(response.context['invitation_code'], Member.objects.get(username=self.username).invitation_code)

    def test_edit_profile(self):
        url = reverse('edit-profile')
        Member.objects.create_superuser(username=self.username, password=self.password,
                                        email=self.email)
        self.client.login(username=self.username, password=self.password)
        data = {
            'email': 'temp@mail.com'
        }
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Member.objects.get(username=self.username).email, 'temp@mail.com')
