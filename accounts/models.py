from random import choice
from string import ascii_uppercase, digits

from django.db import models
from django.contrib.auth.models import AbstractUser


class Member(AbstractUser):
    address = models.TextField(max_length=100, blank=True, null=True)
    mobile = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    image = models.ImageField(upload_to='member_images', null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    job = models.CharField(max_length=100, null=True, blank=True)
    education = models.CharField(max_length=100, null=True, blank=True)
    invitation_code = models.CharField(max_length=5, null=True, blank=True, default="")
    invited_by = models.ForeignKey('Member', on_delete=models.DO_NOTHING, related_name="invitee", null=True, blank=True)

    def create_invitation_code(self):
        is_code_was_generated_before = True
        while is_code_was_generated_before:
            code = ''.join(choice(ascii_uppercase + digits) for i in range(5))
            i = 0
            for x in Member.objects.all():
                i += 1
                if x.invitation_code == code:
                    is_code_was_generated_before = True
                    break
                if i == len(Member.objects.all()):
                    is_code_was_generated_before = False
                    self.invitation_code = code
                    self.save()
                    break
        return self.invitation_code

    def get_invitation_code(self):
        if self.invitation_code:
            return self.invitation_code
        else:
            return self.create_invitation_code()

    def __str__(self):
        return self.username

