from rest_framework import serializers
from rest_framework.fields import CharField

from accounts.models import Member
from FinancialManagement.models import CreditCard


class SignUpSerializer(serializers.ModelSerializer):

    invitee_invitation_code = CharField(source='my_field')

    class Meta:
        model = Member
        fields = ('username', 'invitee_invitation_code')


class SignInSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('username', 'email', 'password')


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('first_name', 'last_name')


class InvitationCodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = 'invitation_code'


class ChargeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CreditCard
        fields = 'asset'
