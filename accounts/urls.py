from django.conf.urls import url
from django.urls import path, include
from django.views.generic import TemplateView

from accounts.views import SignupView, ProfileView, SigninView, EditProfileView, SignoutView,CodeView


urlpatterns = [
    url(r'register/$', SignupView.as_view(), name='registration_register'),
    url(r'new-code/$', CodeView.as_view(), name='new_code'),
    url(r'login/$', SigninView.as_view(), name='registration_login'),
    url(r'logout/$', SignoutView.as_view(), name='registration_logout'),
    path('', include('registration.backends.default.urls')),
    path('profile/', ProfileView.as_view(), name="profile"),
    path('edit-profile/', EditProfileView.as_view(), name="edit-profile"),
]

