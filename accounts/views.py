from random import choice
from string import ascii_uppercase, digits

from django.conf import settings
from django.contrib.auth.views import LoginView, LogoutView
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import TemplateView, UpdateView, FormView
from registration.backends.default.views import RegistrationView, ActivationView

# from accounts.forms import MyRegistrationForm
from accounts.forms import RegisterForm, CodeForm
from accounts.models import Member
from kavenegar import *

from ketabyar.settings import KAVENEGAR_API


def invitation(form):
    mobile = form.cleaned_data['username']
    invitation_code = form.cleaned_data['invitee_invitation_code']
    user = Member.objects.get(username=mobile)
    invitee = Member.objects.get(invitation_code=invitation_code)
    user.invited_by = invitee
    for card in user.credit_card.all():
        card.increase_credit(6000)
    for card in invitee.credit_card.all():
        card.increase_credit(8000)


def send_code(mobile, code):
    try:
        api = KavenegarAPI(KAVENEGAR_API)
        params = {
            'receptor': mobile,  # multiple mobile number, split by comma
            'message': 'کتابیار\n\n کد فعالسازی شما:\n\n' + code,
        }
        response = api.sms_send(params)
        print(response)
    except APIException as error:
        print(error)
        return error
    except HTTPException as error:
        print(error)
        return error


class SignupView(FormView):
    template_name = "accounts/registration/register.html"
    form_class = RegisterForm
    success_url = '/accounts/login/'

    def form_valid(self, form):
        mobile = form.cleaned_data['username']
        code = ''.join(choice(digits) for i in range(5))
        error = None
        if not settings.TESTING:
            error = send_code(mobile, code)
        if error is not None:
            form.add_error('username', 'شماره موبایل وارد شده نامعتبر است.')
            return super().form_invalid(form)
        try:
            user = Member.objects.get(username=mobile)
        except Member.DoesNotExist:
            user = Member.objects.create(username=mobile, password=code)
        user.set_password(code)
        user.save()
        if form.cleaned_data['invitee_invitation_code'] != '':
            invitation(form)
        return super().form_valid(form)


class CodeView(FormView):
    template_name = "accounts/registration/get_new_code.html"
    form_class = CodeForm
    success_url = '/accounts/login/'

    def form_valid(self, form):
        mobile = form.cleaned_data['mobile']
        try:
            user = Member.objects.get(username=mobile)
        except Member.DoesNotExist:
            form.add_error('mobile', 'کاربری با این شماره یافت نشد.')
            return super().form_invalid(form)
        code = ''.join(choice(digits) for i in range(5))
        user.set_password(code)
        user.save()
        send_code(mobile, code)
        return super().form_valid(form)


class SigninView(LoginView):
    template_name = "accounts/registration/login.html"


class SignoutView(LogoutView):
    def get(self, request):
        super().get(request)
        return redirect('registration_login')


class ProfileView(TemplateView):
    template_name = 'accounts/profile.html'

    def get(self, request):
        return redirect("book:book-list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        member = self.request.user
        if member.is_anonymous:
            raise PermissionDenied
        context['email'] = member.email
        return context


class EditProfileView(UpdateView):
    model = Member
    template_name = 'edit-profile.html'
    fields = ['first_name', 'last_name', 'email', 'address', 'mobile', 'birth_date', 'image']

    def get_object(self, queryset=None):
        if self.request.user.is_anonymous:
            raise PermissionDenied
        return self.request.user

    def get_success_url(self):
        return reverse('edit-profile')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_anonymous:
            raise PermissionDenied

        user = self.request.user
        context['invitation_code'] = user.get_invitation_code()
        return context
