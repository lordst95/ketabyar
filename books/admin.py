from django.contrib import admin

from books.models import Book, BorrowBook

admin.site.register(Book)
admin.site.register(BorrowBook)
