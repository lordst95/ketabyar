import datetime

from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import DetailView, UpdateView, ListView, CreateView, TemplateView, FormView

from books.models import Book, BorrowBook


class BookDetail(DetailView):
    model = Book
    template_name = 'book-info.html'


class EditBook(UpdateView):
    model = Book
    template_name = 'edit-book.html'
    fields = ('title', 'age_range', 'writer', 'genre', 'key_words', 'release_date',
              'size', 'volume_number', 'image', 'description', 'summary', 'summary',
              'weekly_price', 'original_price')

    def get_success_url(self):
        pk = self.kwargs.get('pk')
        return reverse('book:book-detail', args=[pk])


class BookList(ListView):
    model = Book
    template_name = 'book-list.html'


class AddNewBook(CreateView):
    model = Book
    template_name = 'add-new-book.html'
    fields = '__all__'

    def get_success_url(self):
        return reverse('book:book-list')


class BorrowBookView(CreateView):
    model = BorrowBook
    template_name = 'borrow-book.html'
    fields = ('deadline', 'book')

    def get_success_url(self):
        return reverse('book:borrowed-book-list')

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        book_pk = kwargs.get('pk')
        book = Book.objects.get(pk=book_pk)
        borrower = request.user
        if book.is_borrowed:
            return HttpResponse("Sorry the book was reserved before")
        if borrower.credit_card.all()[0].asset >= book.weekly_price:
            borrower.credit_card.all()[0].decrease_credit(book.weekly_price)
            book.owner.credit_card.all()[0].increase_credit(book.weekly_price)
        else:
            return HttpResponse("Sorry you should increase your balance")
        now = datetime.datetime.now()
        deadline = now + datetime.timedelta(days=14)
        book.mark_book_as_borrowed()
        BorrowBook.objects.create(book=book, borrower=borrower, deadline=deadline)
        return redirect(to=reverse('book:borrowed-book-list'))


class BorrowedBookListView(ListView):
    model = BorrowBook
    template_name = 'borrowed-book-list.html'


class ReturnBookView(UpdateView):
    model = Book
    fields = '__all__'

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        pk = self.kwargs.get('pk')
        book = Book.objects.get(pk=pk)
        borrow_books = BorrowBook.objects.filter(book=book)
        last_borrowed_book = borrow_books.reverse()[0]
        if request.user != last_borrowed_book.borrower:
            return HttpResponse("you have not borrowed this book")
        last_borrowed_book.book.mark_book_as_returned()
        last_borrowed_book.returned = True
        last_borrowed_book.save()
        return redirect(to=reverse('book:borrowed-book-list'))


class MyBooksView(TemplateView):
    template_name = 'book-list.html'

    def get_context_data(self, **kwargs):
        object_list = Book.objects.filter(owner=self.request.user)
        context = {'object_list': object_list}
        return context


class SearchView(TemplateView):
    template_name = 'book-list.html'

    def get_context_data(self, **kwargs):
        query = self.request.GET.get('query')
        object_list = Book.objects.filter(title__icontains=query)
        context = {'object_list': object_list}
        return context
