import datetime
from django.db import models

from accounts.models import Member
from ketabyar.settings import AGE_RANGES, GENRES, SIZES
from utility.models import Image


class Book(models.Model):

    owner = models.ForeignKey(to=Member, related_name='books', on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=30)
    age_range = models.CharField(choices=AGE_RANGES, max_length=10, blank=True)
    writer = models.CharField(max_length=30, null=True, blank=True)
    genre = models.CharField(choices=GENRES, max_length=50,null=True, blank=True)
    key_words = models.CharField(max_length=50, null=True, blank=True)
    release_date = models.DateField(null=True, blank=True)
    size = models.CharField(choices=SIZES, max_length=10, null=True, blank=True)
    volume_number = models.IntegerField(default=1)
    image = models.ImageField(upload_to='book_images', null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    summary = models.CharField(max_length=200, null=True, blank=True)
    weekly_price = models.IntegerField(default=1000)
    original_price = models.IntegerField(default=1000)
    is_borrowed = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def mark_book_as_borrowed(self):
        self.is_borrowed = True
        self.save()
        return

    def mark_book_as_returned(self):
        self.is_borrowed = False
        self.save()
        return


class BorrowBook(models.Model):
    book = models.ForeignKey(to=Book, on_delete=models.CASCADE, related_name="amanat")
    borrower = models.ForeignKey(to=Member, on_delete=models.CASCADE, related_name="list_amanat_gerefte_shode")
    date = models.DateField(auto_now_add=True)
    deadline = models.DateField()
    number_of_extension = models.IntegerField(default=0)
    returned = models.BooleanField(default=False)

    def calculate_hazine(self):
        num_of_borrowed_week = (datetime.datetime.now().date() - self.date).days/7
        print(datetime.datetime.now().date())
        print(self.date)
        return self.book.weekly_price * num_of_borrowed_week

    def __str__(self):
        return str(self.book).upper()+" is given by "+str(self.book.owner).upper()+" to "+str(self.borrower).upper()
