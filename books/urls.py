from django.urls import path

from books.views import BookDetail, EditBook, BookList, AddNewBook, BorrowedBookListView, BorrowBookView, \
    ReturnBookView, MyBooksView, SearchView

app_name = 'book'

urlpatterns = [
    # ex: /5/
    path('<int:pk>/', BookDetail.as_view(), name='book-detail'),
    path('add/', AddNewBook.as_view(), name='book-add'),
    path('edit/<int:pk>/', EditBook.as_view(), name='book-edit'),
    path('list/', BookList.as_view(), name='book-list'),
    path('borrow/<int:pk>', BorrowBookView.as_view(), name='borrow-a-book'),
    path('return/<int:pk>', ReturnBookView.as_view(), name='return-a-book'),
    path('borrowed-list/', BorrowedBookListView.as_view(), name='borrowed-book-list'),
    path('my-books/', MyBooksView.as_view(), name='my-books'),
    path('search/', SearchView.as_view(), name='book-search'),
]
