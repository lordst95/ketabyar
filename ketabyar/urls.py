from django.conf.urls import url
from django.urls import include, path
from django.contrib import admin
from django.views.static import serve

from ketabyar import settings
from .views import HomePageView

urlpatterns = [
    path('', HomePageView.as_view(), name="homepage"),
    path('accounts/', include('accounts.urls')),
    path('accounting/', include('FinancialManagement.urls')),
    path('book/', include('books.urls', namespace='book')),
    path('apis/', include('apis.urls')),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),  # TODO yadam nemiad ino vase chi gozashtam :))
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
]
