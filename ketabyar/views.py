from django.shortcuts import redirect
from django.views.generic.base import TemplateView


class HomePageView(TemplateView):
    def get(self, request):
        return redirect('book:book-list')

