"""
Django settings for ketabyar project on Heroku. For more info, see:
https://github.com/heroku/heroku-django-template

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
import dj_database_url

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

MEDIA_ROOT = BASE_DIR + '/static/media/'
MEDIA_URL = '/media/'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "80wtj*apbvwm5=#sa*$w(yr-@02by1y%1z_&y8g7+_631crv_u"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Application definition

INSTALLED_APPS = [
    'apis.apps.ApisConfig',
    'accounts.apps.AccountsConfig',
    'django.contrib.admin',
    'django.contrib.sites',     # tebqe recomendation e django-rege...-redux
    'registration', #should be immediately above 'django.contrib.auth' => Note, in order for the templates to properly work, the registration app must appear above django.contrib.auth.
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    # Disable Django's own staticfiles handling in favour of WhiteNoise, for
    # greater consistency between gunicorn and `./manage.py runserver`. See:
    # http://whitenoise.evans.io/en/stable/django.html#using-whitenoise-in-development
    'django.contrib.staticfiles',
    'django_extensions',
    'rest_framework',
    'books',                    #TODO chek kon farq e import be in ravesh ba raveshe apis ke too bala oomade
    'FinancialManagement',
    'utility',
    'widget_tweaks',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'middleware.disable_csrf.DisableCSRFMiddleware',
]

ROOT_URLCONF = 'ketabyar.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'ketabyar', 'base-templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG,
        },
    },
]

WSGI_APPLICATION = 'ketabyar.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTH_USER_MODEL = 'accounts.Member'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Change 'default' database configuration with $DATABASE_URL.
DATABASES['default'].update(dj_database_url.config(conn_max_age=500))

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'staticfiles')
STATIC_URL = '/static/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, 'static'),
]


# config haie django-registration-redux
ACCOUNT_ACTIVATION_DAYS = 5
REGISTRATION_AUTO_LOGIN = True
SITE_ID = 1


# Sending email settings
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'ketabyarApp@gmail.com'
EMAIL_HOST_PASSWORD = 'ketabyartest'
EMAIL_PORT = 587


REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
}

# KAVENEGAR_API = '3737364945755454616E4D3839744E586C414B73446459514C5734494E55372F'
KAVENEGAR_API = '4E6C65535362637A5A444C2F525A4F77634561784F33557273782F414E745072'


AGE_RANGES = (
    ('کودکان', u'کودکان'),
    ('نوجوانان', u'نوجوانان'),
    ('بزرگسالان', u'بزرگسالان'),
)

GENRES = (
    ('مجلات', u'مجلات'),
    ('میکروبوک', u'میکروبوک'),
    ('داستان و رمان', u'داستان و رمان'),
    ('ادبیات', u'ادبیات'),
    ('تبلیغات و بازاریابی', u'تبلیغات و بازاریابی'),
    ('روانشناسی و موفقیت', u'روانشناسی و موفقیت'),
    ('مدیریت', u'مدیریت'),
    ('دین و عرفان', u'دین و عرفان'),
    ('سبک زندگی', u'سبک زندگی'),
    ('زنان و فمنیسم', u'زنان و فمنیسم'),
    ('تاریخ', u'تاریخ'),
    ('فلسفه', u'فلسفه'),
    ('اقتصاد', u'اقتصاد'),
    ('حقوق', u'حقوق'),
    ('کودک', u'کودک'),
    ('نوجوان', u'نوجوان '),
    ('هنر', u'هنر'),
    ('درسی و کمک درسی', u'درسی و کمک درسی'),
    ('دانشگاهی', u'دانشگاهی'),
)

SIZES = (
    ('رقعی', u'رقعی'),
    ('وزیری', u'وزیری'),
    ('وزیری جدید', u'وزیری جدید'),
    ('پالتویی', u'پالتویی'),
    ('رحلی', u'رحلی'),
    ('خشتی', u'خشتی'),
    ('جیبی', u'جیبی'),
)

import sys
TESTING = len(sys.argv) > 1 and sys.argv[1] == 'test'
