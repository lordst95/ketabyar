from django.conf.urls import url
from django.urls import include, path
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from apis import views

urlpatterns = [
    url(r'^sign-up/$', views.SignUpAPIView.as_view(), name="api-sign-up"),
    url(r'^sign-in/$', views.SignInAPIView.as_view(), name="api-sign-in"),
    url(r'^show-profile/', views.ProfileAPIView.as_view(), name='api-profile'),
    url(r'^edit-profile/', views.EditProfileAPIView.as_view(), name='api-edit-profile'),
    url(r'^add-book/', views.AddBookAPIView.as_view(), name='api-add-book'),
    url(r'^show-books/', views.ShowAllBooksAPIView.as_view(), name='api-show-books'),
    url(r'^show-book-info/', views.ShowBookInfoAPIView.as_view(), name='api-show-book-info'),
    url(r'^show-mybooks/', views.ShowMyBooksAPIView.as_view(), name='api-show-mybooks'),
    url(r'^show-my-rented-books/', views.ShowMyRentedBooksAPIView.as_view(), name='api-show-my-rented-books'),
    url(r'^borrow-book/', views.RentBookAPIView.as_view(), name='rent-book'),
    url(r'^show-invitation-code/', views.ShowInvitationCodeAPIView.as_view(), name='api-show-invitation-code'),
    url(r'^show-charge/', views.ShowChargeAPIView.as_view(), name='api-show-charge'),
    url(r'^raise-charge/', views.RaiseChargeAPIView.as_view(), name='api-raise-charge'),


    # add the following URL route to enable obtaining a token via a POST included the user's username and password.
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
]


urlpatterns = format_suffix_patterns(urlpatterns)