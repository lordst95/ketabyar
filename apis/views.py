import json

import datetime
from django.conf import settings
from django.contrib.auth import authenticate, login
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from accounts.models import Member

from accounts.serializers import SignUpSerializer, SignInSerializer, ProfileSerializer, InvitationCodeSerializer, \
    ChargeSerializer

from books.serializers import BookSerializer
from FinancialManagement.models import CreditCard
from rest_framework import generics
from accounts.views import send_code
from books.models import Book, BorrowBook
from random import choice
from string import ascii_uppercase, digits


def invitation(username, code):
    user = Member.objects.get(username=username)
    try:
        invitee = Member.objects.get(invitation_code=code)
        print(invitee)
        user.invited_by = invitee
        user.save()
        for card in user.credit_card.all():
            card.increase_credit(6000)
        for card in invitee.credit_card.all():
            card.increase_credit(8000)
        return False
    except:
        return True


def no_other_choice(request):
    print(request.data)
    try:
        mobile = request.data.get('username')
        print(mobile)
        invitee_invitation_code = request.data.get('invitee_invitation_code')
        print(invitee_invitation_code)
    except:
        i = 0
        temp = tuple()
        for x in request.data.lists():
            i += 1
            if i == 2:
                temp = x
        temp = str(temp[1]).split("\\r\\n\\r\\n")

        try:
            mobile = temp[1].split("\\r\\n")[0]
            invitee_invitation_code = temp[2].split("\\r\\n")[0]
        except:
            mobile = temp[1].split("\\r\\n")[0]
            invitee_invitation_code = ""

    return mobile, invitee_invitation_code


class SignUpAPIView(CreateAPIView):
    serializer_class = SignUpSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        return Member.objects.all()

    def post(self, request, *args, **kwargs):

        mobile, invitee_invitation_code = no_other_choice(request)

        code = ''.join(choice(digits) for i in range(5))
        try:
            Member.objects.get(username=mobile)
            return Response("user ghablan boode")
        except Member.DoesNotExist:
            user = Member.objects.create(username=mobile, password=code)
        user.set_password(code)
        user.save()
        if not settings.TESTING:
            send_code(mobile, code)
        if invitee_invitation_code != "":
            khata = invitation(mobile, invitee_invitation_code)
            if khata:
                return Response("code e ghalat")
        return Response("ok")
        # based on default.views.RegistrationView of registration library


class SignInAPIView(generics.RetrieveAPIView):
    queryset = Member.objects.all()
    serializer_class = SignInSerializer
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        pass

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        password = request.data.get('password')
        print(Member.objects.all()[0].username)
        print(authenticate(username=username, password=password))
        user = authenticate(username=username, password=password)
        login(request, user)
        response = {
            'session': request.session
        }
        return Response(response)


class ProfileAPIView(generics.RetrieveAPIView):
    queryset = Member.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        member = Member.objects.get(username=username)
        response = {
            'first_name': str(member.first_name).encode('utf-8'),
            'last_name': str(member.last_name).encode('utf-8'),
            'username': str(member.username).encode('utf-8'),
            'email': str(member.email).encode('utf-8'),
            'address': str(member.address).encode('utf-8'),
            'mobile': str(member.mobile).encode('utf-8'),
        }
        return Response(response)


class EditProfileAPIView(generics.RetrieveAPIView):
    queryset = Member.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        data = request.data.dict()
        data.pop('username')
        Member.objects.filter(username=username).update(**data)
        return Response("hamechi okay e")


class AddBookAPIView(generics.CreateAPIView):
    serializer_class = BookSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        recieved_data = request.data.dict()
        book = Book.objects.create(**recieved_data)
        book.save()
        return Response("hamechi okay e")


class ShowAllBooksAPIView(generics.RetrieveAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):

        number = int(request.data.get('number'))
        books = Book.objects.all()
        if number < books.__len__() / 10:
            books = books[number * 10:number * 10 + 10]
        else:
            books = []

        serializer = BookSerializer(books, many=True)
        return Response(JSONRenderer().render(serializer.data))


class ShowBookInfoAPIView(generics.RetrieveAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        pk = request.data.get('pk')
        try:
            book = Book.objects.get(pk=pk)
        except Book.DoesNotExist:
            return Response(json.dumps(-1))
        serializer = BookSerializer(book)
        return Response(JSONRenderer().render(serializer.data))


class ShowMyBooksAPIView(generics.RetrieveAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        try:
            books = Book.objects.all().filter(owner__username=username)

        except Book.DoesNotExist:
            return Response(json.dumps(-1))
        serializer = BookSerializer(books, many=True)
        return Response(JSONRenderer().render(serializer.data))


class ShowMyRentedBooksAPIView(generics.RetrieveAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        user = Member.objects.get(username=username)
        borrows = BorrowBook.objects.filter(borrower=user, returned=False)
        books = []
        for borrow in borrows:
            books.append(borrow.book)

        serializer = BookSerializer(books, many=True)
        return Response(JSONRenderer().render(serializer.data))


class RentBookAPIView(generics.RetrieveAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        pk = request.data.get('pk')
        user = Member.objects.get(username=username)
        book = Book.objects.get(pk=pk)
        now = datetime.datetime.now()
        deadline = now + datetime.timedelta(days=14)
        BorrowBook.objects.create(book=book, borrower=user, deadline=deadline)
        serializer = BookSerializer(book, many=False)

        return Response(JSONRenderer().render(serializer.data))


class ShowInvitationCodeAPIView(generics.RetrieveAPIView):
    queryset = Member.objects.all()
    serializer_class = InvitationCodeSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        user = Member.objects.get(username=username)
        code = user.get_invitation_code()
        return Response(json.dumps(code))


class ShowChargeAPIView(generics.RetrieveAPIView):
    queryset = CreditCard.objects.all()
    serializer_class = ChargeSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        try:
            creditcard = CreditCard.objects.get(assosiated_user__username=username)
        except CreditCard.DoesNotExist:
            return Response(json.dumps(-1))
        asset = creditcard.asset;
        return Response(json.dumps(asset))


class RaiseChargeAPIView(generics.RetrieveAPIView):
    queryset = CreditCard.objects.all()
    serializer_class = ChargeSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        raising = request.data.get('raising')

        try:
            creditcard = CreditCard.objects.get(assosiated_user__username=username)
        except CreditCard.DoesNotExist:
            return Response(json.dumps(-1))
        creditcard.asset = creditcard.asset + int(raising)
        creditcard.save()
        return Response(json.dumps("success"))
