from random import choice
from string import ascii_uppercase, digits

from django.db import models


class Image(models.Model):
    image_address = models.ImageField(upload_to='uploads/images', null=True, blank=True)
    image_code = models.CharField(max_length=7, null=True, blank=True)

    def __str__(self):
        return str(self.image_code)

    def create_image_code(self):
        is_code_was_generated_before = True
        while is_code_was_generated_before:
            code = ''.join(choice(ascii_uppercase + digits) for i in range(7))
            i = 0
            for x in self.objects.all():
                i += 1
                if x.image_code == code:
                    is_code_was_generated_before = True
                    break
                if i == len(self.objects.all()):
                    is_code_was_generated_before = False
                    self.image_code = code
                    self.save()
                    break
        return self.image_code
