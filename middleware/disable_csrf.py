from django.utils.deprecation import MiddlewareMixin


class DisableCSRFMiddleware(MiddlewareMixin):
    cookie_header = 'host_store'

    def process_request(self, request):
            setattr(request, '_dont_enforce_csrf_checks', True)
