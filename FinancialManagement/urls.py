from django.urls import path

from FinancialManagement.views import CreditCardView

urlpatterns = [
    path('credit-card/', CreditCardView.as_view(), name='credit-card-view'),
]
