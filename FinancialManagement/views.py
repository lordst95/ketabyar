import random

from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import FormView

from FinancialManagement.forms import CreditCardForm
from FinancialManagement.models import CreditCard


class CreditCardView(FormView):
    template_name = 'credit-card-view.html'
    form_class = CreditCardForm
    success_url = '/accounting/credit-card/'

    def get_context_data(self, **kwargs):
        context = super(CreditCardView, self).get_context_data(**kwargs)
        try:
            context['balance'] = CreditCard.objects.get(assosiated_user=self.request.user)
        except ObjectDoesNotExist:
            return None
        return context

    def form_valid(self, form):
        asset = int(self.request.POST['asset'])
        card = CreditCard.objects.get(assosiated_user=self.request.user)
        card.increase_credit(asset)
        return super().form_valid(form)

