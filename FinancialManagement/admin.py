from django.contrib import admin

from FinancialManagement.models import CreditCard

admin.site.register(CreditCard)
