from django.test import TestCase
from django.urls import reverse
from rest_framework.status import HTTP_200_OK

from FinancialManagement.models import CreditCard
from accounts.models import Member


class AccountingTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.username = '09102030657'
        cls.password = 'gates'
        cls.email = 'johnny@gmail.com'

    def setUp(self):
        super().setUp()
        user = Member.objects.create_superuser(username=self.username, password=self.password,
                                               email=self.email)
        CreditCard.objects.get_or_create(assosiated_user=user)

    def test_checkout_asset(self):
        url = reverse('credit-card-view')
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, HTTP_200_OK)
        user = Member.objects.get(username=self.username)
        credit_card = response.context['balance']
        self.assertEqual(user.credit_card.all()[0].asset, credit_card.asset)

    def test_increasing_asset(self):
        url = reverse('credit-card-view')
        self.client.login(username=self.username, password=self.password)
        data = {
            'asset': '1000'
        }
        response = self.client.post(url, data, follow=True)
        new_balance = response.context['balance'].asset
        self.assertEqual(new_balance, 1000)

