from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from accounts.models import Member


class CreditCard(models.Model):
    assosiated_user = models.ForeignKey(Member, related_name='credit_card', on_delete=models.CASCADE, null=True)
    asset = models.IntegerField(default=0)

    def __str__(self):
        return str(self.asset)+" "+str(self.assosiated_user)

    def increase_credit(self, amount):
        self.asset += int(amount)
        self.save()
        return self.asset

    def decrease_credit(self, amount):
        self.asset -= int(amount)
        self.save()
        return self.asset



# this is signal,should be moved to proper file :))
@receiver(post_save, sender=Member)
def create_credit_card(sender, instance, **kwargs):
    if kwargs['created']:
        CreditCard.objects.create(assosiated_user=instance, asset=0)

