from django.apps import AppConfig


class FinancialmanagementConfig(AppConfig):
    name = 'FinancialManagement'
