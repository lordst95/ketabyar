from django import forms

from FinancialManagement.models import CreditCard


class CreditCardForm(forms.Form):

    class Meta:
        model = CreditCard
        fields = 'asset'
